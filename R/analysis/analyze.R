

analyzePlaquesByNeigh <- function(){
  
  #Read the manually tweaked data with lng lat
  dfAllNeighLngLat  <- readTidyAllNeighTweakedWithLngLat("AllNeighbourhoods")
  
  
  dfByNeigh <- dfAllNeighLngLat %>% 
    group_by(Neighbourhood) %>% 
    dplyr::summarise(numberOfPlaques=n(),
                     numberOfDistinctStreets=n_distinct(Street )
    ) %>% 
    mutate(numberOfPlaquesPerStreet=numberOfPlaques/numberOfDistinctStreets)
  
  dfByNeigh %<>%  rename( `\nNumber of \nplaques` = numberOfPlaques)
  dfByNeigh %<>%  rename( `\nNumber of \ndistinct streets` = numberOfDistinctStreets)
  dfByNeigh %<>%  rename( `\nNumber of \nplaques per \ndistinct streets` = numberOfPlaquesPerStreet)
  
  dfLong <- dfByNeigh %>% reshape2::melt()
  
  #Regular ggplot
  pByNeigh <- ggplot(dfLong) + 
    geom_col(aes(x=Neighbourhood ,y=value, fill=Neighbourhood)) +
    facet_grid(~variable) + 
    # theme(axis.text.x = element_text(angle = 45, hjust = 1)) + 
    # theme(axis.text.x =element_text(size=15))+
    theme(axis.text.y =element_text(size=15)) +
    theme(axis.title =element_text(size=20)) +
    theme(legend.text  =element_text(size=15)) + 
    theme(legend.title   =element_text(size=15)) + 
    ylab("") +  #'value' is not that meaningful
    xlab("") +# the colored legend should suffice
    theme(axis.text.x = element_blank(),
          axis.ticks.x = element_blank(),
          strip.text = element_text(size=15),
          strip.background = element_rect(size=15),
          strip.text.x = element_text(margin = margin(0, 0, .4, 0, "cm")) ) # + #change the size of facet titles
   # coord_fixed(0.3)
  (pByNeigh)
  
  #Plotly
  plotLyByNeigh <- plotly::ggplotly(pByNeigh)
  
  (plotLyByNeigh)
  
  #Save the ggplot
  ggsave(filename = file.path( "Figures", "byNeigh.png"),
         dpi="screen",
         width = 30, 
         height = 30,
         units="cm",
         plot = pByNeigh,
         device = "png")
  
  
  #Also save the html file
  htmlwidgets::saveWidget(plotLyByNeigh,
                          file = here::here(file.path("Leaflet","plaqueStatsPerNeigh.html"))) #html widget absolutely needs absolute paths
  
}



analyzePlaquesByNeigh <- function(){
  
  #Read the manually tweaked data with lng lat
  dfAllNeighLngLat  <- readTidyAllNeighTweakedWithLngLat("AllNeighbourhoods")
  
  #Group by (Street,neighbourhood)
  dfByStreetNeigh <- dfAllNeighLngLat %>% 
    group_by(Street, Neighbourhood) %>% 
    dplyr::summarise(numPlaques=n()) %>% 
    arrange(desc(numPlaques))
 
  dfByStreetNeigh$Street %<>% factor( levels=unique(dfByStreetNeigh$Street %>% as.character()))
  
  #Group only by Street
  dfByStreet <- dfByStreetNeigh %>% 
    group_by(Street) %>% 
    summarise( numPlaques = sum(numPlaques))
 
  pByStreet <- ggplot(dfByStreetNeigh) + 
    geom_col(aes(x=Street ,y=numPlaques, fill=Neighbourhood)) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1)) + 
    theme(axis.text.x =element_text(size=15))+
    theme(axis.text.y =element_text(size=15)) +
    theme(axis.title =element_text(size=20)) +
    theme(legend.text  =element_text(size=15)) + 
    theme(legend.title   =element_text(size=15)) + 
    scale_y_discrete(  limits=1:max(dfByStreet$numPlaques)) +#, labels=1:max(dfByStreet$numPlaques))
    ylab("Number of plaques")
  
  (pByStreet)
  
  ggsave(filename = file.path( "Figures", "byStreet.png"),
         dpi="screen",
         width = 40, 
         height = 30,
         units="cm",
         plot = pByStreet,
         device = "png")
  
}