
 


validateTidy  <- function(){
  
  dfAllNeighLngLat  <- readTidyAllNeighTidyWithLngLat()
  
  distMatMeters <- geosphere::distm( dfAllNeighLngLat %>% dplyr::select(longitude,latitude) ) 
  
  validateDistance(distMatMeters)
  
  
}


validateTweaked  <- function(){
  
  dfAllNeighLngLat  <- readTidyAllNeighTweakedWithLngLat()
  
  distMatMeters <- geosphere::distm( dfAllNeighLngLat %>% dplyr::select(longitude,latitude) ) 
  
  validateDistance(distMatMeters)
  
  
}


#the qc city neighbourhoods are contained within a circle of diameter roughly 35 km + we are only considering 'central' neighbourhoods here
validateDistance <- function(distMatMeters, maxThreshold= 30*10**3){
  
  maxDistMetersPerAdr <-  map_dbl( 1:nrow(distMatMeters), ~max(distMatMeters[.x, ]))   #this is in meters
  maxDistAll <- max(maxDistMetersPerAdr) #for each address, determine the furthest location
  
  if( maxDistAll >maxThreshold) { 
    listPlaquesMaxDist <- map_int( 1:nrow(distMatMeters), ~ which( distMatMeters[.x, ] == maxDistMetersPerAdr[.x]  ) ) %>% unique
    stop(paste0("Fatal error, some plaques are not in qc city, the max distance is ", maxDistAll*10**-3, " km \n" ,
                "Check the following plaques/addresses indices: ", paste(listPlaquesMaxDist, collapse = ",",sep=",")
                )
    )
  } else { 
    print (paste0("All good! max distance is :", maxDistAll*10**-3, " km \n" )) 
  }
  
  
  return(T)
  
}
