import osmnx as ox
import pandas as pd
import numpy as np
import networkx as nx
import os

from OsmnxStreets.definitions import *
from OsmnxStreets.definitions import *

def getQcGraph():
    place=['Québec city, canada']


    graphQc = ox.graph_from_place(place, 
                           simplify=False,
                           retain_all=True)
    
    return(graphQc)


def getQcShp():
    qcCity = ox.gdf_from_place('Québec city, canada')
    
    return(qcCity)



def getUniqueNodes(dfStreetEdges,dummyStart=0):
    
    #Add the neighbourhood data
    dfStreetEdges=addNeighbourhood(dfStreetEdges)
  
    
    #Need same col names if we are to concat rows
    #Add the Street
    dfStreetEdgesRenamedStart= dfStreetEdges.loc[: ,["lngStart", "latStart", "Street"]].rename(columns={"lngStart":"lng", "latStart": "lat"}) 
    dfStreetEdgesRenamedEnd  = dfStreetEdges.loc[: ,["lngEnd", "latEnd", "Street"]].rename(columns={"lngEnd":"lng", "latEnd": "lat"}) 
    
    #Concat/stack rows
    dfConcat=pd.concat( [ dfStreetEdgesRenamedStart,dfStreetEdgesRenamedEnd] ,ignore_index=True)
    
    #Remove duplicates - watch out, with the addition of Street, we need to consider only lng lat 
    #(e.g. to avoid l1,l2,stjeanvieux and l1,l2,stjeansjb)
    idxDup=dfConcat[["lng","lat"]].duplicated()

    dfNodes=dfConcat.loc[~idxDup, ].copy() #.loc returns a view
    numNodesBefore=dfNodes.shape[0]
    dfNodes.reset_index(inplace=True ,drop=True)#watch out with the reset_index .. 
    
    #Select only these columns
    dfNodesWithNeigh=dfNodes[ ["lng","lat","Street" , "Neighbourhood"]] 
    
    #Add a node index (by default indexing starts at 0)
    dfNodesWithNeigh["index"] = [dummyStart+i for i in range(dfNodesWithNeigh.shape[0])]
    numNodesAfter=dfNodesWithNeigh.shape[0]
    
    #Print info on duplicate nodes
    print("removed", numNodesBefore-numNodesAfter, 
          "duplicate rows:\n", 
          dfConcat.loc[ idxDup & dfConcat["Neighbourhood"].isnull(), :],  #this is a little dodgy, 
          "\nThere are ", dfNodesWithNeigh.shape[0], "unique nodes")
    
    

        
    return(dfNodesWithNeigh)


'''
Watch out, this works in place on the original graph (not a copy)
(graph is a pointer and this will change the input graph)

'''
def addNodesGraph(graph,dfStreetEdges):

    #Start at the last existing node index
    numNodes=len(graph.nodes())
    listIndices=[k[0] for k in graph.nodes.items()]
    dummyStart=max(listIndices)+1
    
    dfNodes=getUniqueNodes(dfStreetEdges, dummyStart)
    
    #Order used by osnmx
    #y: lat
    #x: lng
    for i in range(dfNodes.shape[0]): 
         graph.add_node(dummyStart+i , 
                        osmid = dummyStart+i , #add some dummy id?
                        neighbourhood = dfNodes.loc[i,"Neighbourhood"], 
                        y=dfNodes.loc[i,"lat"],   #watch out, the y is the latitude
                        x=dfNodes.loc[i,"lng"]  #watch out, the order seems important (input the lng first)
                         )
            
    return graph, dfNodes;

 
'''
Watch out, this works in place on the original graph (not a copy)
(graph is a pointer and this will change the input graph)

'''
    
def addEdgesGraph(graph,dfEdges):
    
    #Quick test: make sure we are adding edges between existing nodes
    listNodeIndices = [nodeData[0] for nodeData in graph.nodes.data() ]
    nodesInGraph = np.all( np.isin( dfEdges["idxStart"], listNodeIndices) ) & \
    np.all( np.isin( dfEdges["idxEnd"], listNodeIndices) )
    
    if(nodesInGraph==False):
        raise Exception("Fatal error! node indices in dfEdges are not all present in the graph => make sure the indices are correct! ")
    
    numEdgesBefore=len(graph.edges())
    
    for k in range(dfEdges.shape[0]):
        graph.add_edge(dfEdges.loc[k,"idxStart"], 
                       dfEdges.loc[k,"idxEnd"],
                       name="custom_" + dfEdges.loc[k,"Street"],
                       neighbourhood = dfEdges.loc[k,"Neighbourhood"], 
                       key=0,    #set the key to 0 like in the other edges produced by osmnx.graph_from_place
                       highway = "residential", 
                       oneway = False,
                       color="b" ,
                       length=dfEdges.loc[k,"distance"])
        
    numEdgesAfter=len(graph.edges())  
        
    print("Added", numEdgesAfter-numEdgesBefore, " edges to the graph")
        
    #Set the edge colors of other existing edges
    for k,data in enumerate(graph.edges.data()) :
        if  (k<=numEdgesBefore) :
            graph.edges[ data[0],data[1],0] ['color'] ="grey"  #always use index 0
 
 
    return(graph)



def addNeighbourhood(dfStreetEdgesWithDist):
    dfPlaques=pd.read_csv(os.path.join(ROOT_DIR,"Data","Csv","NoLngLat", "AllNeighbourhoods" , "plaquesTidy.csv"),delimiter=",")
    dfStreetEdgesWithNeighDist = pd.merge(dfStreetEdgesWithDist, dfPlaques, on="Street", how="left" )
    
    return(dfStreetEdgesWithNeighDist)


def plotGraph(graph):
    
    ox.project_graph(graph)
    ox.plot_graph(graph)
    
    return(True)



#Check graph nodes (after graph creation) within bbox

def getMaxDict(listVals):
    return( np.max([v for k,v in listVals.items()]) )

def getMinDict(listVals):
    return( np.min([v for k,v in listVals.items()]) )


def getMinMaxLngLat(graph,dfBbox):
    
    #latitude - y
    dictLat=nx.get_node_attributes(graph,"y")

    maxLat=getMaxDict(dictLat)
    maxKey=[ k for k,v in dictLat.items() if v == maxLat]

    minLat=getMinDict(dictLat)
    minKey=[ k for k,v in dictLat.items() if v == minLat]

    print("y -- lat: " , minKey, ":" , minLat ,  " -- " , maxKey, ":", maxLat)
    
    #Longitude - x
    dictLng=nx.get_node_attributes(graph,"x")

    maxLng=getMaxDict(dictLng)
    maxKey=[ k for k,v in dictLng.items() if v == maxLng]

    minLng=getMinDict(dictLng)
    minKey=[ k for k,v in dictLng.items() if v == minLng]

    print("x -- lng: " ,minKey, ":" , minLng ," -- " , maxKey, ":", maxLng   )


    
    #Check within bbox
    
    inBbox =  \
    (dfBbox.loc[0,"lng"]<= minLng) & (minLng <= dfBbox.loc[1,"lng"]) & \
    (dfBbox.loc[0,"lng"]<= maxLng) & (maxLng <= dfBbox.loc[1,"lng"]) & \
    (dfBbox.loc[0,"lat"]<= minLat) & (minLat <= dfBbox.loc[1,"lat"]) & \
    (dfBbox.loc[0,"lat"]<= maxLat) & (maxLat <= dfBbox.loc[1,"lat"])
    
    if (inBbox==False):
        print("Fatal error! not within bbox")
    
    return(inBbox)



def getColorEdgesNodes(graphQcReduced,numEdgesBefore,numNodesBefore):
    dictCol={}
    dictCol["SaintJeanBaptiste"] ="black"
    dictCol["VieuxQuebec"] ="blue"
    dictCol["SaintRoch"] ="green"
    
    addNeighbourhoodNewEdges(graphQcReduced,numEdgesBefore)

    #Nodes
    newColMap = [  "r" for k in range(numNodesBefore) ] + ["b" for k in range(newNodes)]
    
    #Edges
    newColMapEdges = [  "grey" for k in range(numEdgesBefore) ] + \
    [ getCol ( dat[2]["neighbourhood"] ) for k, dat in enumerate(graphQcReduced.edges.data()) if k >= numEdgesBefore]
    
    return(newColMapEdges)


def getCol(s):
    
    try:
        col=dictCol[s]
    except :
        col="grey"
        
    return (col)