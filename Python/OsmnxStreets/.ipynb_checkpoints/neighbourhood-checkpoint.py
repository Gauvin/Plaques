from OsmnxStreets.definitions import * 

import geopandas as gpd
import numpy as np
import os

def readNeighbourhood(filterSJBVieuxQcSaintRoch=True):

    qcCityNeigh = gpd.read_file(os.path.join(ROOT_DIR, "Data","GeoData","Neighbourhoods","vdq-quartier.shp"))
                        
    if (filterSJBVieuxQcSaintRoch) :
        listNeighbourhoods=["Saint-Jean-Baptiste" , "Saint-Roch", "Vieux-Québec/Cap-Blanc/Colline parlementaire"]
        print("Consdidering the following 3 neighbourhoods: ", listNeighbourhoods )
        qcCityNeigh=qcCityNeigh[ np.isin( qcCityNeigh.NOM ,listNeighbourhoods ) ].reset_index()
    else:
        print("Considering ALL neighbourhoods")
    
    return(qcCityNeigh)



def addNeighbourhoodNewEdges(graphQcReduced,numEdgesBefore):
    df=pd.DataFrame.from_items( [  ( "neigh" , [    dat[2]['neighbourhood'] for k, dat in enumerate(graphQcReduced.edges.data()) if k >= numEdgesBefore+1]) ])
    df["Street"]=[   dat[2]['name']  for k, dat in enumerate(graphQcReduced.edges.data()) if k >= numEdgesBefore+1]
    df["col"]=[   getCol(dat[2]['neighbourhood'])  for k, dat in enumerate(graphQcReduced.edges.data()) if k >= numEdgesBefore+1]
    return(df)