 
import osmnx as ox
import pandas as pd
import numpy  as np
import os    
    


'''

2X2 df each row represents the min or max and col is lng or lat

return: 2X3 pandas dataframe (additional column for index=max,min)
'''
def getHardCodedSJB_SaintRoch_VieuxQcBbox_2by2():
    dictBbox={"xmin": -71.24047, 
            "ymin" :  46.79089,
            "xmax" : -71.19298,
            "ymax" : 46.83216 }
    matBbox=np.array([v for k, v in dictBbox.items() ]).reshape(2,2).transpose()
    dict2={}
    dict2["key"] = ["min","max"]
    dict2["lng"] = matBbox[0,:]
    dict2["lat"] = matBbox[1,:]
    dfBbox=pd.DataFrame.from_dict(dict2)
    
    return(dfBbox)

'''

return: pandas dataframe 4X4
'''
def getHardCodedSJB_SaintRoch_VieuxQcBbox_4ExtremePoints():
    
    #Get the min and max lng, lat
    dfBbox=getHardCodedSJB_SaintRoch_VieuxQcBbox_2by2()
    
    #Create the 4 points of the bbox
    dfBbox["dummy"] = np.ones(dfBbox.shape[0])

    dfMerged=pd.merge(dfBbox,dfBbox,
        on="dummy")
    
    #Remove the dummy column
    dfMerged.drop(columns={"dummy"},inplace=True)

    
    return(dfMerged)


'''
Compute hte centroid of the bbox for all 3 neighbourhoods

returns numpy array
'''
def getHardCodedSJB_SaintRoch_VieuxQcBbox_Centroid():
    
    df4ExtremePoints=getHardCodedSJB_SaintRoch_VieuxQcBbox_4ExtremePoints()
    
    df4ExtremePointsLatLng=df4ExtremePoints.loc[:, ["lat_y","lng_x"]]  #permute lat lng for omsnx

    centroid=df4ExtremePointsLatLng.apply( lambda x: np.mean(x))
 
    return( centroid.values )



def getQcGraphSubnet(point,bufferKm=1):
    bufferMeters=bufferKm*10**3
    
    graphQcReduced = ox.graph_from_point(point,distance=bufferMeters) #use a buffer of 10 km around the centroid of the bounding box
    
    return(graphQcReduced)


def getQcGraphSubnetCentroidBbox(bufferKm=1):
    centroid=getHardCodedSJB_SaintRoch_VieuxQcBbox_Centroid()
    
    return(getQcGraphSubnet(centroid,bufferKm))
     




#Make sure within the bouding box
def checkBoxRow(i, dfStreets, dfBox):
    
    try:
        isInBbox=        checkWithinInterval( dfStreets.loc[i, "lngStart"] , dfBox.loc [0,"lng_x"] ,  dfBox.loc [3,"lng_y"] ) & \
        checkWithinInterval( dfStreets.loc[i, "lngEnd"]   , dfBox.loc [0,"lng_x"] ,  dfBox.loc [3,"lng_y"] ) & \
        checkWithinInterval( dfStreets.loc[i, "latStart"] , dfBox.loc [0,"lat_x"] ,  dfBox.loc [3,"lat_y"] ) & \
        checkWithinInterval( dfStreets.loc[i, "latEnd"]   , dfBox.loc [0,"lat_x"] ,  dfBox.loc [3,"lat_y"] )
    
    except Exception as e:
        raise Exception("Fatal error at row " + str(i) + " " + str(e))
    
    return( isInBbox)

  


def checkWithinInterval(x, xmin, xmax):
    return( (x <= xmax) & (x >= xmin))


def checkBoxAll(dfStreets,dfBox):
    
    idxInBbox=[ checkBoxRow( k, dfStreets, dfBox ) for k in range(dfStreets.shape[0]) ]
    inBbox=np.isin(idxInBbox,True)
    
    #Error if any point is not in box i.e. not all in box
    if ~np.all(inBbox) :
        print("Watch out! some of the endpoints of the following streets fall outside the bounding box: ", 
              dfStreets.loc[ ~inBbox, "Street"])
    
 
    return( np.all(inBbox ) )
