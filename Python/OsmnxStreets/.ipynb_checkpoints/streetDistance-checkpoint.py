
import pandas as pd
import geopy.distance
import os
import re


from OsmnxStreets.osmnxBboxSubNetwork import *
from OsmnxStreets.definitions import *



def getDfStreetEdges():
    
    #Read the csv
    dfStreetEdges=pd.read_csv(os.path.join(ROOT_DIR,"Data","Csv","Streets", "uniqueStreetsWithLngLat.csv"),delimiter=",")
    
    #Remove NAs
    dfStreetEdges.dropna(inplace=True)
    
    #Watch out! this requires reindexing to avoid f** ups with .loc[] after
    dfStreetEdges.reset_index(inplace=True)
    
    return(dfStreetEdges)


'''
Make sure given edge (both endpoints) are within the bouding box


returns: bool (true if all nodes defining the street network wall within the bbox)
'''
def checkBoxRow(i, dfStreetEdges, dfBox):
    return( 
        checkWithinInterval( dfStreetEdges.loc[i, "lngStart"] , dfBox.loc [0, "lng_x"] , dfBox.loc [3, "lng_y"] ) &
        checkWithinInterval( dfStreetEdges.loc[i, "lngEnd"]   , dfBox.loc [0,"lng_x"] ,  dfBox.loc [3,"lng_y"] ) &
        checkWithinInterval( dfStreetEdges.loc[i, "latStart"] , dfBox.loc [0,"lat_x"] ,  dfBox.loc [3,"lat_y"] ) &
        checkWithinInterval( dfStreetEdges.loc[i, "latEnd"]   , dfBox.loc [0,"lat_x"] ,  dfBox.loc [3,"lat_y"] )
  )


def checkWithinInterval(x, xmin, xmax):
    return( (x <= xmax) & (x >= xmin))

'''

returns: bool (true if all nodes defining the street network wall within the bbox)
'''
def checkBoxAll(dfStreetEdges,dfBox):
    
    idxInBbox=[ checkBoxRow( k, dfStreetEdges, dfBox ) for k in range(dfStreetEdges.shape[0]) ]
    notInBbox=np.isin(idxInBbox,False)
    print("Watch out! some of the endpoints of the following streets fall outside the bounding box: ", 
          dfStreetEdges.loc[ notInBbox, "Street"])
    
    return( (np.all(notInBbox )) & (len(notInBbox) > 0) ) #can return some empty list for some reason with np.all(notInBbox ) == False


'''

Add the node index to both edges enpoints
Namely useful to match the correct nodes in the networkx graph produced by osmnx
'''
def addStreetDistances(dfStreetEdges, dfNodes):
    
    #Add the start node idx
    dfStreetEdgesMergedStart=pd.merge( dfStreetEdges, dfNodes, 
         how="left",
         left_on=["lngStart","latStart"],
         right_on =["lng","lat"])
    dfStreetEdgesMergedStart.rename(columns={"index_y":"idxStart"} ,inplace=True)


    #Add the end node idx
    dfStreetEdgesMergedStartEnd=pd.merge( dfStreetEdgesMergedStart, dfNodes, 
             how="left",
             left_on=["lngEnd","latEnd"],
             right_on =["lng","lat"])
    dfStreetEdgesMergedStartEnd.rename(columns={"index":"idxEnd"} ,inplace=True)
    
    #Compute the distance
    dfStreetEdgesMergedStartEnd["distance"] = [computeDistanceRow(dfStreetEdgesMergedStartEnd,i) for i in range(dfStreetEdgesMergedStartEnd.shape[0])]
    
    return(dfStreetEdgesMergedStartEnd)
    
    
'''
Computes the default great-circle distance between 2 (lng,lat) (lng2,lat2) points (in meters)

params: pandas df
params: k (row index)

returns : np array


'''
def computeDistanceRow(dfStreetEdgesMergedStartEnd, k):
    return( 
        geopy.distance.distance(dfStreetEdgesMergedStartEnd.loc[k,["lngStart","latStart"]].tolist() , 
                            dfStreetEdgesMergedStartEnd.loc[k,["lngEnd", "latEnd"] ].tolist() )
          ).m



def getDistanceOSM(dfStreetEdgesWithDist, graph):
    
    dist={}
    numErrors=0
    df=pd.DataFrame()

    for strName in dfStreetEdgesWithDist["Street"]:
        dist[strName]=0
        for u,v, edge in graph.edges(data=True):
            try:
                regexRez=re.search('(?<=Boulevard )|(?<=Rue )|(?<=Avenue )+' ,edge["name"]) 
                startPos=regexRez.span()[0]

                strFind=edge["name"][startPos: ]


                ignorecase = re.compile( ".*" + strFind  + ".*", re.IGNORECASE)
                grepResults=ignorecase.match( strName )

                #print(startPos,strCompare ,strName, grepResults)
                if grepResults.group(0) is not None:
                    dist[strName]=dist[strName]+ edge['length']
                df=df.concat(edge)
            except Exception as e:
                #print("error at " , u,v)
                numErrors=numErrors+1
        
    print("There were ", numErrors, " errors in total trying to compute distance with osm edge attribute")
    
    return (dist)



def fixRueStJeanDistance(dfStreetEdgesWithDist,dfStreetEdgesWithNeighDist):
    
    #Get the distance by neighbourhood
    distStJeanSJB,distStJeanVieuxQc =getDistanceRueStJean(dfStreetEdgesWithDist)

    #Adjust the value in the new df
    idxSJB2=~np.isin( [ re.match("rue Saint-Jean sjb", s) for s in  dfStreetEdgesWithNeighDist["Street"]],  None)
    dfStreetEdgesWithNeighDist.loc[idxSJB2 , "distance"]= distStJeanSJB
    dfStreetEdgesWithNeighDist.at[idxSJB2 , "Neighbourhood"]= "SaintJeanBaptiste"

    idxVieuxQc2=~np.isin( [ re.match("rue Saint-Jean vieu*.", s) for s in  dfStreetEdgesWithNeighDist["Street"]],  None)
    dfStreetEdgesWithNeighDist.loc[idxVieuxQc2 , "distance"]= distStJeanVieuxQc
    dfStreetEdgesWithNeighDist.at[idxVieuxQc2, "Neighbourhood"]= "VieuxQuebec"
    
    return(dfStreetEdgesWithNeighDist)



def getDistanceRueStJean(dfStreetEdgesWithDist):
    
    #Fix some bullshit with rue st-jean
    idxSJB=~np.isin( [ re.match("rue Saint-Jean sjb", s) for s in  dfStreetEdgesWithDist["Street"]],  None)
    distStJeanSJB=dfStreetEdgesWithDist.loc[idxSJB , "distance"].values[0]

    idxVieuxQc=~np.isin( [ re.match("rue Saint-Jean vieu*.", s) for s in  dfStreetEdgesWithDist["Street"]],  None)
    distStJeanVieuxQc=dfStreetEdgesWithDist.loc[idxVieuxQc, "distance" ].values[0]

    
    return(distStJeanSJB,distStJeanVieuxQc)
