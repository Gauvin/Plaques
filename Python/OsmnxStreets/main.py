import sys
import os

ROOT_DIR = "/home/charles/Projects/Plaques"
sys.path.append(os.path.join(ROOT_DIR, "Python"))
from OsmnxStreets import *

if __name__ == "__main__":

    graphQcReduced = getQcGraphSubnetCentroidBbox(bufferKm=1)

    #Custom streets to add
    dfStreetEdges = getDfStreetEdges()

    #Nodes
    numNodesBefore = len(graphQcReduced.nodes())
    graphQcReduced, dfNodesWithIndex = addNodesGraph(graphQcReduced, dfStreetEdges)


    #Street dist + neigh
    dfStreetEdgesWithDist = addStreetDistances(dfStreetEdges, dfNodesWithIndex)
    dfStreetEdgesWithNeighDist = addNeighbourhood(dfStreetEdgesWithDist)
    dfStreetEdgesWithNeighDist = fixRueStJeanDistance(dfStreetEdgesWithDist, dfStreetEdgesWithNeighDist)

    #Add edges
    numEdgesBefore = len(graphQcReduced.edges())
    addEdgesGraph(graphQcReduced, dfStreetEdgesWithNeighDist)

    #Color map
    getColorEdgesNodes(graphQcReduced, numEdgesBefore, numNodesBefore)