from OsmnxStreets.definitions import * 

import geopandas as gpd
import pandas as pd
import numpy as np
import os

def readNeighbourhood(filterSJBVieuxQcSaintRoch=True):

    qcCityNeigh = gpd.read_file(os.path.join(ROOT_DIR, "Data","GeoData","Neighbourhoods","vdq-quartier.shp"))
                        
    if (filterSJBVieuxQcSaintRoch) :
        listNeighbourhoods=["Saint-Jean-Baptiste" , "Saint-Roch", "Vieux-Québec/Cap-Blanc/Colline parlementaire"]
        print("Consdidering the following 3 neighbourhoods: ", listNeighbourhoods )
        qcCityNeigh=qcCityNeigh[ np.isin( qcCityNeigh.NOM ,listNeighbourhoods ) ].reset_index()
    else:
        print("Considering ALL neighbourhoods")
    
    return(qcCityNeigh)



def getNeighbourhoodAndColorFromNewEdges(graphQcReduced,numEdgesBefore):



    df=pd.DataFrame.from_items( [  ( "neigh" , [    dat[2]['neighbourhood'] for k, dat in enumerate(graphQcReduced.edges.data()) if k >= numEdgesBefore+1]) ])
    df["Street"]=[   dat[2]['name']  for k, dat in enumerate(graphQcReduced.edges.data()) if k >= numEdgesBefore+1]
    df["col"]=[   getCol(dat[2]['neighbourhood'])  for k, dat in enumerate(graphQcReduced.edges.data()) if k >= numEdgesBefore+1]
    return(df)


def addNeighbourhood(dfStreetEdgesWithDist):


    if "Neighbourhood" in dfStreetEdgesWithDist.columns.values:
        print("In addNeighbourhood => Neighbourhood column already exists ")
        return(dfStreetEdgesWithDist)

    dfPlaques = pd.read_csv(os.path.join(ROOT_DIR, "Data", "Csv", "NoLngLat", "AllNeighbourhoods", "plaquesTidy.csv"),
                            delimiter=",")
    dfStreetEdgesWithNeighDist = pd.merge(dfStreetEdgesWithDist, dfPlaques, on="Street", how="left")

    return (dfStreetEdgesWithNeighDist)


def getColNeighbourhood(s):

    dictCol = {}
    dictCol["SaintJeanBaptiste"] = "black"
    dictCol["VieuxQuebec"] = "blue"
    dictCol["SaintRoch"] = "green"

    try:
        col = dictCol[s]
    except:
        col = "grey"

    return (col)


def getColorEdgesNodes(graphQcReduced, numEdgesBefore, numNodesBefore):


    # Nodes
    #Watch out, the nodes only have 1 key (while the edges have 2) which explains the indexing [1] vs [2]
    newColMapNodes = ["r" for k in range(numNodesBefore)] + \
                     [getColNeighbourhood(dat[1]["neighbourhood"]) for k, dat in enumerate(graphQcReduced.nodes.data()) if k >= numNodesBefore]

    # Edges
    newColMapEdges = ["grey" for k in range(numEdgesBefore)] + \
                     [getColNeighbourhood(dat[2]["neighbourhood"]) for k, dat in enumerate(graphQcReduced.edges.data()) if
                      k >= numEdgesBefore]

    return (newColMapNodes, newColMapEdges)
